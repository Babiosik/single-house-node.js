const express = require('express');
const Validator = require('validatorjs');
const DBAuth = require('../database/auth.js');
const DB = require('../database/database.js');
const DBUser = require('../database/user.js');
const E = require('../errors/errors.js');
const Middleware = require('./middleware.js');

const ruleLogout = {
	token: 'string|size:16',
	bearer: 'required|string|size:32'
};
const ruleChangeLogin = {
	userId: 'required|string|size:16',
	login: 'required|string|between:3,20'
};
const ruleChangePassword = {
	userId: 'required|string|size:16',
	prevPassword: 'required|string|size:64',
	password: 'required|string|size:64'
};
const ruleChangeEmail = {
	userId: 'required|string|size:16',
	email: 'required|email'
};

exports.user = express
	.Router()
	.use(Middleware.auth)
	.get('/user-check', (req, res) => {
		res.status(200).send(JSON.stringify({ status: 'ok' }));
	})
	.get('/logout', Middleware.validation(ruleLogout), (req, res) => {
		if (req.body.token != null) {
			DBAuth.logoutUser(DB, req.body.token);
		} else {
			DBAuth.logoutUser(DB, req.body.bearer);
		}
		res.status(200).send(JSON.stringify({ status: 'ok' }));
	})
	.post('/change/login', Middleware.validation(ruleChangeLogin), (req, res) => {
		if (DB.getUserIdByLogin(req.body.login) != null) {
			return res.status(498).send(E.json(E.loginExist));
		}
		DBUser.updateLogin(req.body.userId, req.body.login);
		res.status(200).send(JSON.stringify({ status: 'ok' }));
	})
	.post('/change/password', Middleware.validation(ruleChangePassword), (req, res) => {
		let success = DBUser.updatePassword(req.body.userId, req.body.prevPassword, req.body.password);
		if (success) {
			res.status(200).send(JSON.stringify({ status: 'ok' }));
		} else {
			res.status(498).send(E.json(E.invalidPassword));
		}
	})
	.post('/change/email', Middleware.validation(ruleChangeEmail), (req, res) => {
		if (DB.getUserIdByEmail(req.body.email) != null) {
			return res.status(498).send(E.json(E.emailExist));
		}
		DBUser.updateEmail(req.body.userId, req.body.email);
		res.status(200).send(JSON.stringify({ status: 'ok' }));
	});
