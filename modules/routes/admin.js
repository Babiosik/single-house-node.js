const express = require('express');
const Validator = require('validatorjs');
const DB = require('../database/database');
const { FileWorker, DBType } = require('../database/file_worker');

const rule = {
	init: {
		password: 'required|string|size:64'
	},
	clearAll: {},
	clear: {
		table: 'required|string'
	},
	show: {
		table: 'required|string'
	}
};

exports.admin = express
	.Router()
	.use(function(req, res, next) {
		let validation = new Validator(req.body, rule.init);
		if (validation.check()) {
			if (req.body.password == 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
				let subvalidation = new Validator(req.body, rule[req.path.substring(1)]);
				if (!subvalidation.check()) {
					let errors = subvalidation.errors.all();
					let errorsSend = [];
					Object.keys(errors).forEach((value, index, arr) => {
						errorsSend.push(errors[value].join('\n'));
					});
					return res.status(400).send(JSON.stringify(errorsSend));
				}
				next();
				return;
			} else {
				res.status(404).send('Not Found 1');
				return;
			}
		}
		console.log(validation.errors);
		console.log(req.body);
		res.status(404).send('Not Found');
	})
	.post('/clearAll', (req, res) => {
		FileWorker.clearAll();
		DB.open();
		res.status(200).send(JSON.stringify({ status: 'ok' }));
	})
	.post('/clear', (req, res) => {
		console.log(req.body);
		FileWorker.clear(req.body.table);
		DB.open();
		res.status(200).send(JSON.stringify({ status: 'ok' }));
	})
	.post('/show', (req, res) => {
		console.log(req.body);
		let table = FileWorker.read(req.body.table);
		res.status(200).send(JSON.stringify(table, null, '\t'));
	});
