// Admin commands [/clearAll ]
exports.admin = require('./admin.js').admin;

// /login /register
exports.auth = require('./auth.js').auth;

// /user-check /logout
exports.user = require('./user.js').user;
