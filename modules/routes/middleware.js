const DB = require('../database/database');
const Validator = require('validatorjs');

module.exports = class Middleware {
	static validation(rule) {
		return (req, res, next) => {
			let validator = new Validator(req.body, rule);
			if (validator.check()) return next();

			let errors = validator.errors.all();
			let errorsSend = [];
			Object.keys(errors).forEach((value, index, arr) => {
				errorsSend.push(errors[value].join('\n'));
			});
			return res.status(499).send(JSON.stringify(errorsSend));
		};
	}
	static auth(req, res, next) {
		let token = req.headers.accesstoken;
		if (token == undefined || token == null || !token.startsWith('Beerer ')) {
			res.status(401).send('Access denied');
			return;
		}
		token = token.substring(7);
		let user = DB.getUserIdByToken(token);
		if (user == null) {
			res.status(401).send('Access denied');
			return;
		}
		DB.updateSession(token);
		req.body.userId = user;
		req.body.bearer = token;
		next();
	}
};
