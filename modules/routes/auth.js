const express = require('express');
const Validator = require('validatorjs');
const DBAuth = require('../database/auth.js');
const DB = require('../database/database.js');
const E = require('../errors/errors.js');
const Middleware = require('./middleware.js');

const ruleLogin = {
	login: 'required|string|between:3,20',
	password: 'required|string|size:64'
};
const ruleReg = {
	login: 'required|string|between:3,20',
	password: 'required|string|size:64',
	invite: 'required|string|size:16',
	email: 'required|email'
};

exports.auth = express
	.Router()
	.post('/login', Middleware.validation(ruleLogin), (req, res) => {
		let token = DBAuth.authUser(DB, req.body.login, req.body.password);
		if (token == null) {
			return res.status(498).send(E.json(E.invalidLogin));
		}
		res.status(200).send(JSON.stringify({ token: token }));
	})
	.post('/register', Middleware.validation(ruleReg), (req, res) => {
		if (!DBAuth.validInviteCode(DB, req.body.invite)) {
			return res.status(498).send(E.json(E.invalidInvite));
		}
		if (DB.getUserIdByLogin(req.body.login) != null) {
			return res.status(498).send(E.json(E.loginExist));
		}
		if (DB.getUserIdByEmail(req.body.email) != null) {
			return res.status(498).send(E.json(E.emailExist));
		}
		DBAuth.registryUser(DB, req.body.login, req.body.email, req.body.password, req.body.invite);
		let token = DBAuth.authUser(DB, req.body.login, req.body.password);
		if (token == null) {
			return res.status(498).send(E.json(E.invalidLogin));
		}
		res.status(200).send(JSON.stringify({ token: token }));
	});
