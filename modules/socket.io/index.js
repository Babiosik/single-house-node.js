const Validator = require('validatorjs');
const DB = require('../database/database.js');

const ruleMessageSend = {
	to: 'required|string|size:16',
	from: 'required|string|size:32',
	message: 'required|string'
};

function validation(data, rule) {
	let validator = new Validator(req.body, rule);
	if (validator.check()) return null;

	let errors = validator.errors.all();
	let errorsSend = [];
	Object.keys(errors).forEach((value, index, arr) => {
		errorsSend.push(errors[value].join('\n'));
	});
	return errorsSend;
}

class SocketIO {
	constructor(server) {
		this.socketIO = require('socket.io')(server);
		this.socketIO.on('connection', function(client) {
			console.log('Connected...', client.id);
			
			//listens for new messages coming in
			client.on('message', function name(data) {
				console.log(data);
				this.socketIO.emit('message', data);
			});

			//listens when a user is disconnected from the server
			client.on('disconnect', function() {
				console.log('Disconnected...', client.id);
			});
			
			//listens when there's an error detected and logs the error on the console
			client.on('error', function(err) {
				console.log('Error detected', client.id);
				console.log(err);
			});

			// data { from (accessToken), to (userId), message (string) }
			// return emit message_read, message_receive
			client.on('message_send', function(data) {
				let result = validation(data, ruleMessageSend);
				if (result != undefined) {
					client.emit('message_error', result);
					return;
				}
				let userId = DB.getUserIdByToken(data.from);
				if (userId == undefined) {
					client.emit('message_error', {
						error: 'pizda 401',
					});
					return;
				}
				client.emit('message_receive', {
					from: userId,
					messagee: data.message,
				});
			});
		});
		setInterval(() => this.socketIO.emit('time', new Date().toTimeString()), 1000);
	}
}

exports.SocketIO = SocketIO;