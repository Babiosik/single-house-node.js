module.exports.json = (code) => {
	return JSON.stringify({
		code: code
	});
};
module.exports.invalidInvite = 1;
module.exports.loginExist = 2;
module.exports.emailExist = 3;
module.exports.invalidLogin = 4;
module.exports.invalidPassword = 5;
