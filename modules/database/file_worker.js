const fs = require('fs');
const path = 'database/';

const dbType = {
	Users: 'users',
	Tokens: 'tokens',
	Sessions: 'sessions',
	Unread: 'unread'
};

module.exports.DBType = dbType;
module.exports.FileWorker = class FileWorker {
	static init() {
		// if (!fs.existsSync(path + 'unread')) fs.mkdirSync(path + 'unread');
		if (!fs.existsSync(path + 'users.json')) this.clear(dbType.Users);
		if (!fs.existsSync(path + 'tokens.json')) this.clear(dbType.Tokens);
		if (!fs.existsSync(path + 'sessions.json')) this.clear(dbType.Sessions);
		if (!fs.existsSync(path + 'unread.json')) this.clear(dbType.Unread);
	}

	static read(type) {
		return JSON.parse(fs.readFileSync(path + type + '.json'));
	}

	static save(type, data) {
		fs.writeFile(path + type + '.json', JSON.stringify(data, null, '\t'), () => console.log('Saved: ' + type));
	}

	static clear(type) {
		fs.writeFileSync(path + type + '.json', initFile(type));
		console.log('Clear: ' + type);
	}

	static clearAll() {
		fs.rmSync(path + 'users.json');
		fs.rmSync(path + 'tokens.json');
		fs.rmSync(path + 'sessions.json');
		fs.rmSync(path + 'unread.json');
		this.init();
		console.log('Clear all');
	}
};

function initFile(type) {
	let data;
	switch (type) {
		case dbType.Users:
			data = {
				'0000000000000000': {
					login: '123',
					email: 'admin@gg.co',
					password: 'a1e48daec54145146b89d816a089ba3294d2748796b8491e9a719d54d2ca0b8a',
					invite: 'FFFFFFFFFFFFFFFF'
				}
			};
			break;
		case dbType.Tokens:
			data = {
				FFFFFFFFFFFFFFFF: {
					owner: '0000000000000000'
				},
				'0000000000000000': {
					owner: null
				},
				'1111111111111111': {
					owner: null
				},
				'2222222222222222': {
					owner: null
				},
				'3333333333333333': {
					owner: null
				},
				'4444444444444444': {
					owner: null
				},
				'5555555555555555': {
					owner: null
				},
				'6666666666666666': {
					owner: null
				},
				'7777777777777777': {
					owner: null
				},
				'8888888888888888': {
					owner: null
				},
				'9999999999999999': {
					owner: null
				}
			};
			break;
		case dbType.Sessions:
			data = {};
			break;
		case dbType.Unread:
			data = {};
			break;
	}
	return JSON.stringify(data, null, '\t');
}
