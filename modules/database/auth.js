const crypto = require('crypto');
module.exports = class DBAuth {
	static validInviteCode(db, invite = '') {
		let inviteLock = db.tokens[invite] != undefined && db.tokens[invite] != null && db.tokens[invite].owner == null;
		return inviteLock;
	}

	static registryUser(db, login = '', email = '', password = '', invite = '') {
		let id = '';
		do {
			id = crypto.randomBytes(8).toString('hex');
		} while (db.users[id] != undefined);
		db.users[id] = {
			login: login,
			email: email,
			password: password,
			invite: invite
		};
		db.tokens[invite].owner = id;
		db.saveDB(3);
	}
	static authUser(db, login = '', password = '') {
		let userId = db.getUserIdByLogin(login);
		if (userId == null || db.users[userId].password != password) return null;

		let rand = '';
		do {
			rand = crypto.randomBytes(16).toString('hex');
		} while (db.sessions[rand] != undefined);
		db.sessions[rand] = {
			userId: userId
		};
		db.saveDB(4);
		return rand;
	}
	static logoutUser(db, token) {
		db.sessions[token] = undefined;
		db.saveDB(4);
	}
};
