const DB = require('./database');

module.exports = class DBUser {
	static updateLogin(userId = '', login = '') {
		DB.users[userId].login = login;
		DB.saveDB(1);
	}
	static updatePassword(userId = '', prevPassword = '', password = '') {
		if (DB.users[userId].password != prevPassword) {
			return false;
		}
		DB.users[userId].password = password;
		DB.saveDB(1);
		return true;
	}
	static updateEmail(userId = '', email = '') {
		DB.users[userId].email = email;
		DB.saveDB(1);
	}
};
