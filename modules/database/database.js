const FileWorker = require('./file_worker.js');
const Auth = require('./auth.js');
const fw = FileWorker.FileWorker;
const dbType = FileWorker.DBType;

class DB {
	static init(callback = () => {}) {
		fw.init();
		this.open();
	}

	static open() {
		this.users = fw.read(dbType.Users);
		this.unread = fw.read(dbType.Unread);
		this.tokens = fw.read(dbType.Tokens);
		this.sessions = fw.read(dbType.Sessions);
	}

	static saveDB(arrayId = 0) {
		if (arrayId & 1) {
			fw.save(dbType.Users, this.users); // 0001
		}
		if (arrayId & 2) {
			fw.save(dbType.Tokens, this.tokens); // 0010
		}
		if (arrayId & 4) {
			fw.save(dbType.Sessions, this.sessions); // 0100
		}
		if (arrayId & 8) {
			fw.save(dbType.Unread, this.unread); // 1000
		}
	}

	static getUserIdByToken(token = '') {
		let login = this.sessions[token];
		if (login == undefined || login == null) {
			return null;
		}
		return login.userId;
	}
	static getUserIdByLogin(login = '') {
		const keys = Object.keys(this.users);
		for (const key in keys) {
			if (this.users[keys[key]].login == login) return keys[key];
		}
		return null;
	}
	static getUserIdByEmail(email = '') {
		let keys = Object.keys(this.users);
		for (const key in keys) {
			if (this.users[keys[key]].email == email) return keys[key];
		}
		return null;
	}
	static updateSession(token = '') {
		let login = this.sessions[token];
		if (login == undefined || login == null) {
			return null;
		}
		this.sessions[token].lastUpdate = Date.now();
		return login.userId;
	}
}

module.exports = DB;

// UserModel
// userId {
//   login
//   email
//   password (hash256)
//   invite (hash256)
// }

// InviteModel
// token {
//   owner (null / userId)
// }

// SessionModel
// token {
//   userId
//   lastUpdate (timestamp)
// }

// UnreadModel
// userId {
//   date_send (timestamp)
//   from (userId)
//   message (string)
// }
