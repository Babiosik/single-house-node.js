

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const PORT = process.env.PORT || 5000;

const router = require('./modules/routes/index.js');
const { exit } = require('process');
const DB = require('./modules/database/database.js');
DB.init();

const server = express()
	.use(bodyParser.json())
	.use(
		bodyParser.urlencoded({
			extended: true
		})
	)
	.use(express.static(path.join(__dirname, 'public')))
	.set('views', path.join(__dirname, 'views'))
	.set('view engine', 'ejs')
	.get('/', (req, res) => res.render('pages/index'))
	.use('/ad', router.admin)
	.use('/api', router.auth)
	.use('/api/user', router.user)
	.listen(PORT, () => console.log(`Listening on ${PORT}`));

const socketType = require('./modules/socket.io/index.js').SocketIO;
const socket = new socketType(server);

